<div class="toc">
<ul>
<li><a href="#stages-of-hacking">Stages of Hacking</a><ul>
<li><a href="#reconnaissance">Reconnaissance</a></li>
<li><a href="#scanning-and-enumeration">Scanning and Enumeration</a></li>
<li><a href="#gaining-access">Gaining Access</a></li>
<li><a href="#maintaining-access">Maintaining Access</a></li>
<li><a href="#covering-tracks">Covering Tracks</a></li>
</ul>
</li>
<li><a href="#malware">Malware</a></li>
<li><a href="#resources">Resources</a><ul>
<li><a href="#online-tools">Online Tools</a></li>
<li><a href="#software">Software</a><ul>
<li><a href="#scripts-for-script-kiddies">Scripts for script kiddies</a></li>
<li><a href="#linux-shell">Linux Shell</a></li>
<li><a href="#operating-systems">Operating Systems</a></li>
<li><a href="#virtualization">Virtualization</a></li>
</ul>
</li>
<li><a href="#video-instructions">Video Instructions</a></li>
<li><a href="#online-documentations">Online Documentations</a><ul>
<li><a href="#other-resources">Other Resources</a></li>
</ul>
</li>
<li><a href="#online-competitions">Online Competitions</a></li>
</ul>
</li>
<li><a href="#dictionary">Dictionary</a></li>
</ul>
</div>


# Stages of Hacking

[Lockheed Martin Cyber-Killchain](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html):
1. Reconaissance
2. Weaponisation
3. Delivery
4. Exploitation
5. Installation
6. Command & Control
7. Actions on Objectives


## Reconnaissance
Spying on a target (system or person) and gain as much information as possible.

* OSINT (Open Source Intelligence)
  * [Google Dorks](https://www.exploit-db.com/google-hacking-database) /
    [Shodan](https://www.shodan.io/)
  * [URLCrazy](https://www.morningstarsecurity.com/research/urlcrazy) /
    [The Harvester](https://github.com/laramies/theHarvester)
* Footprinting
  * [Maltego](http://paterva.com/web6/products/maltego.php) ([Kali](https://tools.kali.org/information-gathering/maltego-teeth)) /
    [Recon-ng](http://recon-ng.com/) ([Kali](https://tools.kali.org/information-gathering/recon-ng)) / 
	[Foca](https://www.elevenpaths.com/labstools/foca/index.html)
* Passive
  * Location Information
    * Satellite images
    * Building layout (badge readers, security, fencing, etc.)
  * Job Information
    * Employees (name, job title, phone number, manager, etc.)
    * Pictures (badge photos, desk photos, computer photos, etc.)
  * Target Validation (`whois`, 
					`nslookup`, 
					[dnsrecon](https://tools.kali.org/information-gathering/dnsrecon))
  * Finding Subdomains (Google Fu,
						`dig`, 
						`nmap`, 
						[sublist3r](https://tools.kali.org/information-gathering/sublist3r), 
						[Bluto](https://github.com/darryllane/Bluto), 
						[crt.sh](https://crt.sh/), 
						etc.)
  * Data Breaches (HaveIBeenPwned, etc.)  
* Active (*caution: don't get caught*)
  * Fingerprinting (nmap, [wappalyzer](https://www.wappalyzer.com/), WhatWeb, BuiltWith, Netcat)
  * Use Proxys (

## Scanning and Enumeration
* Port-Scanning (Information about target system, Searching for known security breaches)
  * Active systems in a network
  * Active / open ports.
  * Network components / structure
  * Details about Operating Systems (Banner Grabbing).
  * Uptime (last patch?)
  * Search for IDS (Intrusion Detection Systems)
  * Networkdiagram ([Netzwerkrechner](https://www.heise.de/netze/tools/netzwerkrechner/))
* Tools (`ping`, `hping3`, 
		 [Nmap](https://tools.kali.org/information-gathering/nmap), 
		 [Zenmap](https://nmap.org/zenmap/),
		 [PingPlotter](https://www.pingplotter.com/),
		 [MultiPing](https://www.multiping.com/),
		 [nikito](https://tools.kali.org/information-gathering/nikto))
* Important Ports:

| 20/21  | 22   | 23   | 25  | 49  | 53  | 80  | 110 | 143 | 389 | 443 | 445 |
| ---    | ---  | ---  | --- | --- | --- | --- | --- | --- | --- | --- | --- | 
| FTP  | SSH | Telnet | SMTP | Login | DNS | HTTP | POP3 | IMAP | LDAP | HTTPS | SMB (Microsoft-DS) |

## Gaining Access
* Reverse Shell etc.
  * e.g. [MSFvenom Payload Creator](https://tools.kali.org/exploitation-tools/msfpc)

## Maintaining Access
* Lateral Movement (moving to interesting systems)
* Privilege Escalation (gain administrator access)
* Hashdump (get hashed passwords of other accounts)
* Backdoor (regain connection after reboot)

## Covering Tracks
* Erase Logfiles (Clearlogs.exe, Ccleaner.exe, MRU-Blaster)
  * Disable auditing (Audipol.exe)
  * Administrator should save logfiles offsite to protect them!
* Migrate Meterpreter process with some other process (to remove from task list)
```
meterpreter> migrate -P 2644
[*] Migrating from 1600 to 2644...
[*] Magration completed successfully.
```


# Malware
* Kinds of Malware: Trojan, Virus, Backdoor, Worm, Rootkit, Spyware, Ransomware, Botnet-Client, Adware


# Resources

## Online Tools
* [Amiunique](https://amiunique.org/) (Browser Fingerprint)
* Domain Tools
  * [Internet Root Servers](https://root-servers.org/) 
  * [DNS Tools](http://www.dnstools.ch/visual-traceroute.html)
  * [Whois](https://whois.domaintools.com/)
  * [Urlscan](http://urlscan.io/)
* Search Engines
  * [Netcraft](https://searchdns.netcraft.com/)
  * [Exploit DB](https://www.exploit-db.com/)
  * [Sploitus](https://sploitus.com/)
  * [Shodan](https://www.shodan.io/)
  * [Hunter.io](https://hunter.io/) (E-Mail Adresses)
* Analyze Malware
  * [Any.run](https://any.run/) (sandbox for malware)
  * [Hybrid Analysis](https://www.hybrid-analysis.com/) (sandbox malware analysis service)
  * [Cuckoo](https://cuckoo.cert.ee/) (sandbox for malware, local installation possible)
  * [Virustotal](https://www.virustotal.com/) (malware scanner)
* [SSL Labs](https://www.ssllabs.com/) (Vulnerability Scanner)
* [PenTest](https://pentest.ws) (Database for pentester)
* [Common Vulnerabilities and Exposures](https://cve.mitre.org/)
* [Rainbow Table](https://crackstation.net/) (for non-salted hashed passwords)
* [Try It Online](https://tio.run/#) (online interpreters)

## Software
* [Traceroute Mapper](https://stefansundin.github.io/traceroute-mapper/)
* [Spiderfoot](https://www.spiderfoot.net/) (Reconnaissance)
* [Veil Framework](https://www.veil-framework.com/)
* Commercial Vulnerability Scanner
  * [Nessus](https://tenable.com)
  * [GFI LanGuard](https://www.gfi.com/)
* [Kali Tools](https://tools.kali.org/tools-listing)
  * [Metasploit](https://www.metasploit.com)
	  ([Kali](https://tools.kali.org/exploitation-tools/metasploit-framework), 
	  [Commands](https://www.offensive-security.com/metasploit-unleashed/msfconsole-commands/))
  * [Maltego](http://paterva.com/web6/products/maltego.php) ([Kali](https://tools.kali.org/information-gathering/maltego-teeth))
  * [Recon-ng](http://recon-ng.com/) ([Kali](https://tools.kali.org/information-gathering/recon-ng))
  * [Open Vulnerability Assessment Scanner](http://www.openvas.org/) ([Kali](https://tools.kali.org/vulnerability-analysis/openvas)) -
  OpenVAS is a framework of several services and tools offering a comprehensive and powerful vulnerability scanning and vulnerability management solution.
* [EvilPortals](https://github.com/kbeflo/evilportals) (for phishing attacks against WiFi clients)

### Scripts for script kiddies
* [Mimikatz](https://github.com/gentilkiwi/mimikatz) (Password tool)
* [Threader3000](https://github.com/dievus/threader3000) (Python port scanner)
* [nmap_vulners](https://github.com/vulnersCom/nmap-vulners) (Addon for nmap to show CVEs)
* [Impacket](https://github.com/SecureAuthCorp/impacket) (collection of Python classes for working with network protocols)
* [Linux smart enumeration](https://github.com/diego-treitos/linux-smart-enumeration)
* [Scapy](https://scapy.net/) (Packet crafting for Python)
  * [Awesome Scapy](https://github.com/secdev/awesome-scapy) (List with extra tools, add-ons, articles and cool exploits)
* Joomla
  * [Joomblah](https://github.com/mavr1k7/Joomblah) (SQLi for Joomla! 3.7.0)
  * [Joomscan](https://github.com/rezasp/joomscan) (Joomla! Vulnerability Scanner)

### Linux Shell
* [Shell Cheat Sheet](https://i.kinja-img.com/gawker-media/image/upload/s--Gv2Bd7Kg--/c_scale,f_auto,fl_progressive,q_80,w_800/drn5gedukwnkq04nufma.png)
* [Oneliners](https://github.com/stephenturner/oneliners) (`awk`, `sed`, etc.)
* [Linux Privilege Escalation through SUDO](https://github.com/TH3xACE/SUDO_KILLER)
* [Reverse Shell Cheat Sheet](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)
* [GTFObins](https://gtfobins.github.io/) (useful shell commands)
* [Rawsec's CyberSecurity Inventory](https://inventory.rawsec.ml/tools.html)

### Operating Systems
* [Kali Linux](https://kali-linux.org)
* [Parrot Security OS](https://parrotlinux.org)
* [Complete Mandiant Offensive VM (Commando VM)](https://github.com/fireeye/commando-vm)
* [BackBox](https://www.backbox.org)
* [BlackArch](https://blackarch.org)
* [Pentoo](https://www.pentoo.ch)
* [Network Security Toolkit](https://networksecuritytoolkit.org/nst/index.html)
* [Fedora Security Spin](https://spins.fedoraproject.org/)
* [ArchStrike](https://archstrike.org/)
* [Qubes OS](https://www.qubes-os.org/)

### Virtualization
* [Virtual Box](https://www.virtualbox.org/)
* [VMWare Workstation Player](https://www.vmware.com/products/workstation-player.html)
* [Activate Virtualization in BIOS](https://2nwiki.2n.cz/pages/viewpage.action?pageId=75202968 )

## Video Instructions
* [Full Ethical Hacking Course - Beginner Network Penetration Testing (2019)](https://www.youtube.com/watch?v=WnN6dbos5u8) (15hrs)
* [OpenSecurityTraining playlists](https://www.youtube.com/user/OpenSecurityTraining/playlists)
* [CyberSec Lounge Taining Pages](http://cybersec-lounge.com/index.html)
* [Security Talks](https://github.com/PaulSec/awesome-sec-talks)
* [Jacob Sorber Youtube Channel](https://www.youtube.com/user/jms36086) (Professor at Clemson University)
* [MalwareAnalysisForHedgehogs](https://www.youtube.com/c/MalwareAnalysisForHedgehogs)
* [OALabs](https://www.youtube.com/channel/UC--DwaiMV-jtO-6EvmKOnqg)
* [LiveOverflow](https://liveoverflow.com/)

## Online Documentations
* [How to Build a Cybersecurity Career](https://danielmiessler.com/blog/build-successful-infosec-career/)
* Writeups
  * [Youtube IppSec Channel](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA) (Walkthrouh HackTheBox Challenges)
  * [CyberSec Padawan](https://www.cybersecpadawan.com) (Walkthrouh hacks)
  * [Writeups for HacktheBox machines](https://github.com/Hackplayers/hackthebox-writeups)
  * [bug bounty writeups](https://pentester.land/list-of-bug-bounty-writeups.html)
* Books
  * [OWASP Testing Guide 4.0](https://www.owasp.org/images/1/19/OTGv4.pdf)
  * [Reverse Engineering for Beginners](https://beginners.re/)
  * [Free Programming books](https://books.goalkicker.com/)
* Online Courses
  * [Malware Analysis Course](https://github.com/RPISEC/Malware) (by RPISEC)
  * [OpenSecurityTraining](http://opensecuritytraining.info/Training.html)
* Linklists and knowledge collections
  * [Nothink!](https://www.nothink.org/) (Linklist of CVEs and auxiliaries)
  * [Payloads All The Things](https://github.com/swisskyrepo/PayloadsAllTheThings)
  * [Privacy Tools](https://www.privacytools.io/providers/)
  * [HackTricks](https://book.hacktricks.xyz/)
  * [Helpful notes for CTF](https://github.com/JohnHammond/ctf-katana)
  * [Services and Tools wiki](https://0x00sec.org/t/services-tools-wiki/2123)
  * [The Bug Hunters Methodology](https://github.com/jhaddix/tbhm)
  * [Transitioning Servicemembers Cheat Sheet](https://cheatography.com/xfaith/cheat-sheets/transitioning-servicemembers/)
* [Awesome lists](https://github.com/sindresorhus/awesome) / [Lists](https://github.com/jnv/lists)
  * [Capture the Flag](https://github.com/apsdehal/awesome-ctf)
  * [Cyber Skills](https://github.com/joe-shenouda/awesome-cyber-skills)
  * [Hacking](https://github.com/carpedm20/awesome-hacking)
  * [Malware Analysis](https://github.com/rshipp/awesome-malware-analysis)
  * [Penetration Testing](https://github.com/fabacab/awesome-pentest#readme)
  * [Privacy](https://github.com/Igglybuff/awesome-piracy)
  * [Security](https://github.com/sbilly/awesome-security)
    * [Android Security](https://github.com/ashishb/android-security-awesome#readme)
	* [Application Security](https://github.com/paragonie/awesome-appsec#readme)
	* [Cyber Security Blue Team](https://github.com/fabacab/awesome-cybersecurity-blueteam#readme)
    * [Personal Security Checklist](https://github.com/Lissy93/personal-security-checklist)
	* [OWASP Web Application Security Testing Checklist](https://github.com/0xRadi/OWASP-Web-Checklist)
  * [Vehicle Security](https://github.com/jaredthecoder/awesome-vehicle-security#readme)
  * [Web Security](https://github.com/qazbnm456/awesome-web-security#readme)
* Coding Tutorials
  * [Ruby Programming Language - Full Course](https://www.youtube.com/watch?v=t_ispmWmdjY) (4 hrs)
  * [Learn Python](https://www.learnpython.org/)
  * [Eloquent JavaScript](https://eloquentjavascript.net/)
  * [You Don't Know JS Yet (book series)](https://github.com/getify/You-Dont-Know-JS)

### Other Resources
* [SecLists](https://github.com/danielmiessler/SecLists)
* [1.4 billion clear text passwords](https://github.com/philipperemy/tensorflow-1.4-billion-password-analysis) (41 GB [magnet](magnet:?xt=urn:btih:7ffbcd8cee06aba2ce6561688cf68ce2addca0a3&dn=BreachCompilation&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fglotorrents.pw%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337) link for torrent download)

## Online Competitions
* [Try Hack Me](https://tryhackme.com/)
* [HacktheBox](https://www.hackthebox.eu/) (vulnerable machines)
* [VulnHub](https://www.vulnhub.com/) (vulnerable VMs)
* [Codewars](https://www.codewars.com) (Coding Problems)
* [Project Euler](https://projecteuler.net) (Math Problems)


# Dictionary
* Zero Day Attack: using a bug in an app before developers can fix it (usually just a few days available).
* Vulnerability: existence of a bug, error or wrong implementation that affects the system security.
* Lateral Movement: using an existing access to move through a system (usually stay within same privileges).
* Exploit: script that uses (*exploits*) a bug in an IT system.
* Bug Bounty: program by a company to find bugs in their products.
* Doxing: release information about people that has been collected from public databases / social media.
* Payload: code to upload and execute on an infected system (creates backdoor, deletes data, etc.)
* Social Engineering: interact with users and using them to help with your attack.


# License
[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)  
To the extent possible under law, the person who associated [CC0](https://creativecommons.org/publicdomain/zero/1.0/) with this work has waived all copyright and related or neighboring rights to this work. 